﻿using Xamarin.Forms;

namespace AppServiceWCF.Interfaces
{
    public interface ILodingPageService
    {
        void InitLoadingPage(ContentPage loadingIndicatorPage = null);

        void ShowLoadingPage();

        void HideLoadingPage();
    }
}