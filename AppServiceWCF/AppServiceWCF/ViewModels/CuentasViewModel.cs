﻿using System.Collections.Generic;

namespace AppServiceWCF.ViewModels
{
    public class CuentasViewModel : BaseViewModel
    {
        private FBS_BilleteraBICO.RespuestaListadoCuentas respuesta;

        public List<string> Lista { get; set; }

        public CuentasViewModel(FBS_BilleteraBICO.RespuestaListadoCuentas cuentas)
        {
            this.respuesta = cuentas;
            this.Init();
        }

        private void Init()
        {
            Title = this.respuesta.ListadoCuentas[0].NombreCompleto.ToLower();
            this.Lista = new List<string>();
            foreach (var c in this.respuesta.ListadoCuentas)
            {
                this.Lista.Add(c.NumeroDeCuenta);
            }
        }
    }
}