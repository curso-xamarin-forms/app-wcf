﻿using AppServiceWCF.Interfaces;
using AppServiceWCF.Views;
using System;
using System.ServiceModel;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace AppServiceWCF.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        #region Atributos

        public string celular;
        public string identificacion;

        #endregion Atributos

        #region Propiedades

        public string Celular
        {
            get
            {
                return celular;
            }
            set
            {
                SetProperty(ref celular, value);
            }
        }

        public string Identificacion
        {
            get
            {
                return identificacion;
            }
            set
            {
                SetProperty(ref identificacion, value);
            }
        }

        #endregion Propiedades

        #region Servicios

        private FBSServicesClient _client;

        #endregion Servicios

        #region Constructores

        public AboutViewModel()
        {
            Title = "Consulta de Cuentas";

            ConsultarCommand = new Command(Consultar);
            this.InitializeFBSServicesClient();
        }

        #endregion Constructores

        private void InitializeFBSServicesClient()
        {
            _client = new FBSServicesClient(
                new BasicHttpBinding(),
                new EndpointAddress("http://cv.cpmv.fin.ec:9010/FBS_BilleteraBICO.FBSServices.svc"));
            _client.ListarCuentasCompleted += _client_ListarCuentasCompleted;
        }

        #region Comandos

        public ICommand ConsultarCommand { get; private set; }

        #endregion Comandos

        #region Metodos

        private async void Consultar()
        {
            if (string.IsNullOrEmpty(this.Celular) || string.IsNullOrEmpty(this.Identificacion))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Ingrese los datos", "OK");
                return;
            }
            try
            {
                DependencyService.Get<ILodingPageService>().ShowLoadingPage();
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    this._client.ListarCuentasAsync(this.Celular, this.Identificacion);
                }
                else
                {
                    DependencyService.Get<ILodingPageService>().HideLoadingPage();
                    await Application.Current.MainPage.DisplayAlert("Error", "No tiene conexión a Internet", "OK");
                }
            }
            catch (Exception e)
            {
                DependencyService.Get<ILodingPageService>().HideLoadingPage();
                await Application.Current.MainPage.DisplayAlert("Error", e.Message, "OK");
            }
        }

        #endregion Metodos

        #region Eventos

        private void _client_ListarCuentasCompleted(object sender, ListarCuentasCompletedEventArgs e)
        {
            if (e.Error != null || e.Cancelled)
            {
                this.Cerrar();
            }
            else
            {
                var resp = e.Result;
                if (!resp.EstadoTransaccion.Equals("ERROR"))
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        var cuentas = new CuentasPage(new CuentasViewModel(resp));
                        ((MainPage)Application.Current.MainPage).Detail.Navigation.PushAsync(cuentas);
                        this.Cerrar();
                    });
                }
                else
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        this.Cerrar();
                        Application.Current.MainPage.DisplayAlert("Error", resp.MensajeTransaccion, "OK");
                    });
                }
            }
        }

        private void Cerrar()
        {
            DependencyService.Get<ILodingPageService>().HideLoadingPage();
        }

        #endregion Eventos
    }
}