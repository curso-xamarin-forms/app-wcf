﻿using AppServiceWCF.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppServiceWCF.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CuentasPage : ContentPage
    {
        CuentasViewModel info;

        public CuentasPage(CuentasViewModel model)
        {
            InitializeComponent();

            this.BindingContext = this.info = model;
        }
    }
}